package com.example.rujuls.parkinglotmanager.Menu;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.rujuls.parkinglotmanager.Adapters.PassAdapter;
import com.example.rujuls.parkinglotmanager.Adapters.VehicleInfo;
import com.example.rujuls.parkinglotmanager.Adapters.passInfo;
import com.example.rujuls.parkinglotmanager.Database.DBHandler;
import com.example.rujuls.parkinglotmanager.Database.PassStore;
import com.example.rujuls.parkinglotmanager.Pass.PassChoiceActivity;
import com.example.rujuls.parkinglotmanager.Pass.PassDetailsActivity;
import com.example.rujuls.parkinglotmanager.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Handler;

public class PassListActivity extends AppCompatActivity {

    ListView passList;
    String device_id;
    DBHandler d;
    String dura;
    String pri;
    public static final String PASS_NAME ="com.example.rujuls.parkinglotmanager._PASS";
    public static final String PASS_NO ="com.example.rujuls.parkinglotmanager._NO";
    public static final String PASS_DUR ="com.example.rujuls.parkinglotmanager._DUR";
    public static final String PASS_TYPE ="com.example.rujuls.parkinglotmanager._TYPE";
    public static final String PASS_UNIQUE ="com.example.rujuls.parkinglotmanager._UNIQUE";
    public static final String COLUMN_PASS = "PASS";
    public static final String COLUMN_PASS_NO = "UNINO";
    public static final String TABLE_PASS = "PassDataSheet";
    public static final String COLUMN_PASS_PRICE = "PRICE";
    private SwipeRefreshLayout swipe;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_list);

        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_pass);

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                passListSetup();
            }
        });


        d = new DBHandler(getApplicationContext());
        passListSetup();
        //d.deletePass();
    }

    public void passListSetup(){

        String[] pass = {"1-Month","3-Month","6-Month","1-Year"};

        passList = (ListView) findViewById(R.id.pass_list);

        PassAdapter passAdapter = new PassAdapter(this.getApplicationContext(), R.layout.pass_row);

        SQLiteDatabase db = d.getWritableDatabase();
        String query = ("SELECT * FROM " + TABLE_PASS );
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            String passName = cursor.getString(cursor.getColumnIndex(d.COLUMN_PASS));
            String type = cursor.getString(cursor.getColumnIndex(d.COLUMN_PASS_TYPE));
            String hid = cursor.getString(cursor.getColumnIndex(d.COLUMN_PASS_NO));
            passInfo info = new passInfo(passName,type,hid);
            passAdapter.add(info);
            //cal = doWork(entry);
            cursor.moveToNext();
        }
        passList.setAdapter(passAdapter);
        passAdapter.notifyDataSetChanged();
        passList.setOnItemClickListener(onListClick);
        postString("https://api.plvm.in/pass/list");
        swipe.setRefreshing(false);
    }

    private AdapterView.OnItemClickListener onListClick = new AdapterView.OnItemClickListener(){
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            ViewGroup vg = (ViewGroup) view;
            TextView t2 = (TextView) vg.findViewById(R.id.pass_no);
            TextView t1 = (TextView) vg.findViewById(R.id.pass_name);
            TextView t3 = (TextView) vg.findViewById(R.id.hidden);
            String unique = t3.getText().toString();
            SQLiteDatabase db = d.getWritableDatabase();
            String query = ("SELECT * FROM " + TABLE_PASS + " WHERE " +COLUMN_PASS_NO+ "=\"" +unique+ "\";");
            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();

            while (!cursor.isAfterLast()){
                 dura = cursor.getString(cursor.getColumnIndex(d.COLUMN_PASS_DURATION));
                 pri = cursor.getString(cursor.getColumnIndex(d.COLUMN_PASS_PRICE));
                cursor.moveToNext();
            }

            Intent intent = new Intent(getApplication(), PassDetailsActivity.class);
            intent.putExtra(PASS_NAME,t1.getText().toString());
            intent.putExtra(PASS_TYPE,t2.getText().toString());
            intent.putExtra(PASS_DUR,dura);
            intent.putExtra(PASS_NO,pri);
            intent.putExtra(PASS_UNIQUE,unique);
            startActivity(intent);
        }
    };

    void postString(String url){

        TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        device_id = tm.getDeviceId();
        final TextView t ;
        d = new DBHandler(getApplicationContext());
        d.getWritableDatabase();
        RequestQueue queue = Volley.newRequestQueue(this);

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //t.setText("Response is: "+ response);
                        String JsonFinal = response;
                        try {
                            JSONArray parentArray = new JSONArray(JsonFinal);
                            String pass_name=null;
                            String unique_no=null;
                            String dur=null;
                            String type=null;
                            String price=null;
                            StringBuffer finalBuffer = new StringBuffer();
                            for (int i=0; i<parentArray.length(); i++) {
                                JSONObject finalObject = parentArray.getJSONObject(i);

                                pass_name = finalObject.getString("name");
                                unique_no = finalObject.getString("unique_no");
                                type = finalObject.getString("vehicle_type");
                                dur = finalObject.getString("duration");
                                price = finalObject.getString("price");
                                finalBuffer.append(pass_name + "-" + unique_no + "-" + type + "-" + dur + "-" + price + "\n" );
                                int n = d.addUpdatePass(unique_no);
                                if(n==0) {
                                    PassStore pass = new PassStore(pass_name, unique_no, dur, price, type);
                                    d.addPassProduct(pass);
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        }
                    }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //t.setText("That didn't work!");
                Toast.makeText(PassListActivity.this, "PLEASE CHECK NET CONNECTION", Toast.LENGTH_SHORT).show();
            }
        }){

            protected Map<String,String> getParams(){

                Map<String,String> params = new HashMap<String,String>();

                params.put("imei", device_id);
                return params;

            }
        };
// Add the request to the RequestQueue.
        queue.add(stringRequest);

    }


}
