package com.example.rujuls.parkinglotmanager.Main;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.rujuls.parkinglotmanager.Database.DBHandler;
import com.example.rujuls.parkinglotmanager.Menu.PassListActivity;
import com.example.rujuls.parkinglotmanager.R;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
//import com.example.rujuls.parkinglotmanager.VehListActivity;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;


public class MainActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    DBHandler d;
    String device_id;
    private Tracker mTracker;
    String name = "Main";
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;

    // needed for communication to bluetooth device / network
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;

    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;

    // Standard SerialPortService ID
    UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        d = new DBHandler(this);
        //d.deleteAllErrorProduct();
        //d.deleteProduct();

                // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        AnalyticsApp application = (AnalyticsApp) getApplication();
        mTracker = application.getDefaultTracker();


            //findBT();
           //openBT();


       // mTracker = ((AnalyticsApplication)this.getApplication()).getsTracker(AnalyticsApplication.TrackerName.APP_TRACKER);
        //mTracker.setScreenName("Screen~" + name);
        //mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    // this will find a bluetooth printer device
    void findBT() {
        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if(mBluetoothAdapter == null) {

            }
            if(!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBluetooth, 0);
            }
            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            if(pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    if (device.getName().equals("BlueTooth Printer")) {
                        mmDevice = device;
                        break;
                    }
                }
            }
           // Toast.makeText(this, "Bluetooth device found.", Toast.LENGTH_LONG);
            //myLabel.setText("Bluetooth device found.");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    // tries to open a connection to the bluetooth printer device
    void openBT() throws IOException {
        try {
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            if (!mmSocket.isConnected())
                mmSocket.connect();
            mmOutputStream = mmSocket.getOutputStream();
            mmInputStream = mmSocket.getInputStream();
            beginListenForData();
            Toast.makeText(this,"Bluetooth Opened",Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void beginListenForData() {
        try {
            final Handler handler = new Handler();
            // this is the ASCII code for a newline character
            final byte delimiter = 10;
            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];
            workerThread = new Thread(new Runnable() {
                public void run() {
                    while (!Thread.currentThread().isInterrupted() && !stopWorker) {
                        try {
                            int bytesAvailable = mmInputStream.available();
                            if (bytesAvailable > 0) {
                                byte[] packetBytes = new byte[bytesAvailable];
                                mmInputStream.read(packetBytes);
                                for (int i = 0; i < bytesAvailable; i++) {
                                    byte b = packetBytes[i];
                                    if (b == delimiter) {
                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length
                                        );
                                        // specify US-ASCII encoding
                                        final String data = new String(encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;
                                        // tell the user data were sent to bluetooth printer device
                                        handler.post(new Runnable() {
                                            public void run() {
//                                                myLabel.setText(data);
                                            }
                                        });
                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }
                        } catch (IOException ex) {
                            stopWorker = true;
                        }
                    }
                }
            });
            workerThread.start();
            //printData();
            Toast.makeText(this,"BEGIN LISTENING",Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }






    //GO TO PASS ACTIVITY
    public void addPass(){
        Intent i = new Intent(this,PassListActivity.class);
        startActivity(i);

    }

    protected void onStart(){
        super.onStart();
        mTracker.setScreenName("Screen~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    //MENU CODE AND TABBED HEADER
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_pass) {
            addPass();
            return true;
        }

        if (id == R.id.action_about) {
            Toast.makeText(this,"CREATED BY WEBSITE ENGINEER",Toast.LENGTH_LONG).show();
            return true;
        }

        if (id == R.id.action_upload) {
            //RESENDING FAILED EXITS
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    d = new DBHandler(getApplicationContext());
                    Cursor c = d.sendErrorExit();
                    if(c.getCount()!=0) {
                        do {
                            String passVeh = c.getString(c.getColumnIndex(d.ERR_NAME));
                            String cell = c.getString(c.getColumnIndex(d.ERR_CELL));
                            String type = c.getString(c.getColumnIndex(d.ERR_TYPE));
                            String entry = c.getString(c.getColumnIndex(d.ERR_ENTRY));
                            String date = c.getString(c.getColumnIndex(d.ERR_EXIT));
                            String price = c.getString(c.getColumnIndex(d.ERR_PRICE));
                            String dur = c.getString(c.getColumnIndex(d.ERR_DUR));
                                exitErrorString("https://api.plvm.in/vehicle/exit", passVeh, cell, type, entry, date, price, dur);
                        } while (c.moveToNext());
                    }
                }
            };
            Thread thread = new Thread(r);
            thread.run();
            d = new DBHandler(getApplicationContext());
            Cursor c = d.sendErrorExit();
            Toast.makeText(getApplication(),"Pending Entries: "+c.getCount(), Toast.LENGTH_LONG).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    //EXIT DATA TO SERVER
    void exitErrorString(String url, final String veh, final String cell,final String type, final String finalEntry, final String finalExit,final String price, final String dur){

        TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        device_id = tm.getDeviceId();

        RequestQueue queue = Volley.newRequestQueue(this);

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //status.setText("Response is: "+ response);
                        if(response.matches("Success")){
                            d = new DBHandler(getApplicationContext());
                            d.deleteOneProductExit(veh);
                            d.deleteErrorProduct(veh);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

               // Toast.makeText(getApplication(),error.toString(),Toast.LENGTH_LONG).show();

                d = new DBHandler(getApplicationContext());
                d.deleteOneProductExit(veh);
            }
        }){

            protected Map<String,String> getParams(){

                Map<String,String> params = new HashMap<String,String>();

                params.put("imei", device_id);
                params.put("entry",finalEntry);
                if(cell != null)params.put("mobileno",cell);
                params.put("type",type);
                params.put("vehicleno",veh);
                params.put("exit", finalExit);
                params.put("price",price);
                params.put("duration",dur);

                return params;

            }
        };
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            if(getArguments().getInt(ARG_SECTION_NUMBER)==2)
            {

                View rootView = inflater.inflate(R.layout.fragment_vehicle_list, container, false);
                return rootView;

            }

            else
            {

                View rootView = inflater.inflate(R.layout.fragment_entry, container, false);
                return rootView;
            }

        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        ExitFragment exit;
        EntryFragment entry;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch(position) {
                case 0:
                return entry.newInstance();
                case 1 :
                    return exit.newInstance();
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "ENTRY";
                case 1:
                    return "EXIT";

            }
            return null;
        }
    }
}
