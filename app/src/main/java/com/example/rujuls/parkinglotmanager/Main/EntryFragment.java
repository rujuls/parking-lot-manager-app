package com.example.rujuls.parkinglotmanager.Main;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextUtils;

import android.os.Environment;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.telephony.TelephonyManager;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.example.rujuls.parkinglotmanager.Database.DBHandler;
import com.example.rujuls.parkinglotmanager.Database.DataStore;
import com.example.rujuls.parkinglotmanager.R;
import android.view.WindowManager;
import com.google.android.gms.analytics.GoogleAnalytics;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import com.example.rujuls.parkinglotmanager.Main.AnalyticsApp;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


/**
 * A simple {@link Fragment} subclass.
 */
public class EntryFragment extends Fragment{

    private static String TAG = "com.example.rujuls.parkinglotmanager";
     View v;
    Spinner spinner;
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    TextView VehNo;
    TextView TypeInput;
    static String  finalType;
    private MainActivity.SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    EditText one;
    DBHandler d;
    String device_id;
    String type;
    EditText MobileNo;
    long cell;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    Tracker mTracker;
    private TextInputLayout TextVehLayout, TextMobLayout;

    BluetoothAdapter mBluetoothAdapter;


    // needed for communication to bluetooth device / network
    Thread workerThread;

    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;

    public static EntryFragment newInstance() {
        // Required empty public constructor
        EntryFragment entry = new EntryFragment();
        return entry;
    }
    UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");

    public EntryFragment(){}
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
       v = inflater.inflate(R.layout.fragment_entry, container, false);
        //Snackbar mySnackbar1 = Snackbar.make(v, "Upload Successfull",Snackbar.LENGTH_LONG );
        //mySnackbar1.show();

        ArrayAdapter vehType = ArrayAdapter.createFromResource(getActivity(), R.array.wheeler,android.R.layout.simple_spinner_item);
       // ArrayAdapter<String> vehType = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item,values);
        vehType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner=(Spinner) v.findViewById(R.id.spinner);
        spinner.setAdapter(vehType);
        //Toast.makeText(getActivity(),"selected" ,Toast.LENGTH_LONG).show();
        //Log.i(TAG,"Yo");
        AnalyticsApp application = (AnalyticsApp) getActivity().getApplication();
        mTracker = application.getDefaultTracker();


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                 TypeInput = (TextView) view;
                type = parent.getItemAtPosition(i).toString();
                setType(type);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        TextVehLayout = (TextInputLayout) v.findViewById(R.id.input_vehicle);
        one = (EditText) v.findViewById(R.id.editText);
        //one.addTextChangedListener(new MyTextWatcher(one));

            findBT();
            //openBT();


        Button b = (Button) v.findViewById(R.id.button);
        b.setOnClickListener(mButtonClickListener);
        return v;
    }


    private View.OnClickListener mButtonClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            onEntryEnter(v);
        }
    };

    public void setType(String boob){

        finalType = boob;
    }

    public String getType(){

        return finalType;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Set screen name.
        mTracker.setScreenName("Entry Fragment");

// Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    // this will find a bluetooth printer device
    void findBT() {
        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if(mBluetoothAdapter == null) {

            }
            if(!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBluetooth, 0);
            }
            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            if(pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    if (device.getName().equals("BlueTooth Printer")) {
                        mmDevice = device;
                        break;
                    }
                }
            }
            // Toast.makeText(this, "Bluetooth device found.", Toast.LENGTH_LONG);
            //myLabel.setText("Bluetooth device found.");
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    void openBT() throws IOException {
        try {
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            if (!mmSocket.isConnected())
                mmSocket.connect();
            mmOutputStream = mmSocket.getOutputStream();
            mmInputStream = mmSocket.getInputStream();
            beginListenForData();
            Toast.makeText(getContext(),"Bluetooth Opened",Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void beginListenForData() {
        try {
            final Handler handler = new Handler();
            // this is the ASCII code for a newline character
            final byte delimiter = 10;
            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];
            workerThread = new Thread(new Runnable() {
                public void run() {
                    while (!Thread.currentThread().isInterrupted() && !stopWorker) {
                        try {
                            int bytesAvailable = mmInputStream.available();
                            if (bytesAvailable > 0) {
                                byte[] packetBytes = new byte[bytesAvailable];
                                mmInputStream.read(packetBytes);
                                for (int i = 0; i < bytesAvailable; i++) {
                                    byte b = packetBytes[i];
                                    if (b == delimiter) {
                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length
                                        );
                                        // specify US-ASCII encoding
                                        final String data = new String(encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;
                                        // tell the user data were sent to bluetooth printer device
                                        handler.post(new Runnable() {
                                            public void run() {
//                                                myLabel.setText(data);
                                            }
                                        });
                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }
                        } catch (IOException ex) {
                            stopWorker = true;
                        }
                    }
                }
            });
            workerThread.start();
            //printData();
            Toast.makeText(getContext(),"BEGIN LISTENING",Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    //ENTRY ENTER
    public void onEntryEnter(View view){




        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        SimpleDateFormat rd = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dd = new SimpleDateFormat("dd/MM/yyyy");


        TextMobLayout = (TextInputLayout) v.findViewById(R.id.input_mobile);

        Date systemDate = Calendar.getInstance().getTime();
        String myDate = sdf.format(systemDate);
        String realDate = rd.format(systemDate);
        String storeDate =  df.format(systemDate);
        String displayDate =  dd.format(systemDate);
        d = new DBHandler(getActivity());

        String veh = one.getText().toString();
        MobileNo = (EditText) v.findViewById(R.id.CellNo);
        EntryFragment e = new EntryFragment();
        String no = MobileNo.getText().toString();


       // MobileNo.addTextChangedListener(new MyTextWatcher(MobileNo));



        if(no.matches("")|| no.length()!=10){

            TextMobLayout.setErrorEnabled(true);
            MobileNo.setError(getString(R.string.mob_error));
            if(veh.matches("")){TextVehLayout.setErrorEnabled(true);
                one.setError(getString(R.string.veh_error));}
        }
        else if(veh.matches("")){TextVehLayout.setErrorEnabled(true);
            one.setError(getString(R.string.veh_error));}
        else{
            TextVehLayout.setErrorEnabled(false);
            TextMobLayout.setErrorEnabled(false);
            cell = Long.parseLong(no);
            String adv = "";
            type = e.getType();
            if(type.matches("CYCLE")){
                finalType = "2-cycle";
                adv = "RS.20";
            }
            if(type.matches("BIKE")){
                finalType = "2-bike";
                adv = "RS.20";
            }
            if(type.matches("RICKSHAW")){
                finalType = "3-rickshaw";
                adv = "RS.20";
            }
            if(type.matches("CAR")){
                finalType = "4-car";
                adv = "RS.40";
            }
            if(type.matches("TRUCK")){
                finalType = "4-truck";
                adv = "RS.40";
            }
            if(type.matches("BUS")){
                finalType = "4-bus";
                adv = "RS.40";
            }

            //STORING ENTRIES IN DB AND SENDING TO SERVER METHOD

                DataStore f = new DataStore(veh,storeDate,finalType,cell);

                d.addProduct(f);

                Cursor cs = d.fetch();
                exportToExcel(cs);

                String finalDate = realDate + " " + myDate;
                Toast.makeText(getActivity(),"Entry Saved" ,Toast.LENGTH_LONG).show();
                postString("https://api.plvm.in/vehicle/entry",veh,finalDate,finalType,no);

            try {
                openBT();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            //BLUETOOTH PRINTING CODE
            try {
                Toast.makeText(getContext(),"PRINTTTTTT!!!!!!!!!!!!!!!!!",Toast.LENGTH_SHORT).show();

//                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
//                byte[] image = Utils.decodeBitmap(bitmap);
//                mmOutputStream.write(image);

                String titleStr = "\n          www.plvm.in\n";
                titleStr += "            RECEIPT\n";
                titleStr += "       I.P. MALL PAY & PARK\n";
                titleStr += "          BORIVALI (W)\n";
                titleStr += "********************************\n";
                titleStr += "      TIME: 10:00 AM TO 10.30 PM\n";
                titleStr += "       NIGHT PARKING NOT ALLOWED\n";
                titleStr += "       VEHICLE TYPE: " +type+ "\n";
                titleStr += "       ADVANCE AMOUNT: " +adv+ "\n";
                titleStr += "       VEHICLE NO : "+ veh +"\n";
                titleStr += "       IN DATE : "+ displayDate +"\n";
                titleStr += "       IN TIME : "+ myDate +"\n";
                titleStr += "********************************\n";
                titleStr += "     4W 2H RS.40 AFT RS.20/H\n";
                titleStr += "     2W 2H RS.20 AFT RS.10/H\n";
                titleStr += "          PAY IN FIRST\n";
                titleStr += "     PARKING AT OWNER'S RISK\n";
                titleStr += "     NOT RESPONSIBLE FOR HELMET\n";

                mmOutputStream.write(titleStr.getBytes());

//                Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.advert);
//                byte[] image2 = Utils.decodeBitmap(bitmap1);
//                mmOutputStream.write(image2);
//
//                Bitmap bitmap2 = BitmapFactory.decodeResource(getResources(), R.drawable.advert2);
//                byte[] image3 = Utils.decodeBitmap(bitmap2);
//                mmOutputStream.write(image3);

                String titleStr1 = "\n\n\n";
                mmOutputStream.write(titleStr1.getBytes());
                mmOutputStream.write("".getBytes());
                mmOutputStream.write("".getBytes());

                Toast.makeText(getContext(),"Printed Reciept",Toast.LENGTH_LONG).show();
            } catch (Exception error) {
                error.printStackTrace();
            }

            //RESENDING FAILED ENTRIES
            Cursor c = d.sendErrorEntry();
            if(c.getCount()!=0){
                do{
                    String entry    = c.getString(c.getColumnIndex(d.COLUMN_NAME));
                    String date = c.getString(c.getColumnIndex(d.COLUMN_ENTRY));
                    String type = c.getString(c.getColumnIndex(d.COLUMN_TYPE));
                    String cell = c.getString(c.getColumnIndex(d.COLUMN_CELL));
                    String finalTime;
                    SimpleDateFormat _24HourSDF = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                    SimpleDateFormat _12HourSDF = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
                    try {
                        Date _12Time = _12HourSDF.parse(date);
                        finalTime = _24HourSDF.format(_12Time);
                        postString("https://api.plvm.in/vehicle/entry",entry,finalTime,type,cell);
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                }while(c.moveToNext());
            }

            try {
                closeBT();
            } catch (IOException errr) {
                errr.printStackTrace();
            }
        }

        // Get tracker.
// Build and send an Event.
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Entries")
                .setAction("Get Entries")
                .setLabel("Entry")
                .build());
    }

    // close the connection to bluetooth printer.
    void closeBT() throws IOException {
        try {
            stopWorker = true;
            mmOutputStream.close();
            mmInputStream.close();
            mmSocket.close();
            Toast.makeText(getActivity(),"BT Closed" ,Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //ENTRY DATA TO SERVER
    void postString(String url, final String veh, final String finalDate, final String type, final String cell){

        TelephonyManager tm = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        device_id = tm.getDeviceId();
        //final TextView t ;
        //t = (TextView) v.findViewById(R.id.textView13);
        RequestQueue mRequestQueue;
// Instantiate the cache
        Cache cache = new DiskBasedCache(getActivity().getCacheDir(), 1024 * 1024); // 1MB cap
// Set up the network to use HttpURLConnection as the HTTP client.
        Network network = new BasicNetwork(new HurlStack());
// Instantiate the RequestQueue with the cache and network.
        mRequestQueue = new RequestQueue(cache, network);
// Start the queue
        mRequestQueue.start();

        // RequestQueue queue = Volley.newRequestQueue(this);

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        if(response.matches("Success")){
                           // t.setText("Data Upload Successful");
                            Toast.makeText(getActivity(),"Data Uploaded",Toast.LENGTH_LONG);
                            one = (EditText) v.findViewById(R.id.editText);
                            MobileNo = (EditText) v.findViewById(R.id.CellNo);
                            one.setText("");
                            MobileNo.setText("");
                            d.updateEntryStatus(veh);
                            Snackbar mySnackbar = Snackbar.make(v.findViewById(R.id.entry_cord), "Upload Successfull",Snackbar.LENGTH_LONG );
                            mySnackbar.show();
                        }
                        else{ Snackbar mySnackbar = Snackbar.make(v.findViewById(R.id.entry_cord), "Upload Failed",Snackbar.LENGTH_LONG );
                            mySnackbar.show();
                            Toast.makeText(getActivity(),"Upload Failed",Toast.LENGTH_LONG);}
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //t.setText("Data Upload Failed");
                if(error instanceof NetworkError){Snackbar mySnackbar = Snackbar.make(v.findViewById(R.id.entry_cord), "Network Error",Snackbar.LENGTH_LONG );
                    mySnackbar.show();}
                else if (error instanceof ParseError){Snackbar mySnackbar = Snackbar.make(v.findViewById(R.id.entry_cord), "Parse Error",Snackbar.LENGTH_LONG );
                    mySnackbar.show();}
                else if (error instanceof ServerError){Snackbar mySnackbar = Snackbar.make(v.findViewById(R.id.entry_cord), "Server Error",Snackbar.LENGTH_LONG );
                    mySnackbar.show();}
                else if (error instanceof AuthFailureError){Snackbar mySnackbar = Snackbar.make(v.findViewById(R.id.entry_cord), "Authentication Error",Snackbar.LENGTH_LONG );
                    mySnackbar.show();}
                else if (error instanceof TimeoutError){Snackbar mySnackbar = Snackbar.make(v.findViewById(R.id.entry_cord), "Time Out Error",Snackbar.LENGTH_LONG );
                    mySnackbar.show();}
                else{Snackbar mySnackbar = Snackbar.make(v.findViewById(R.id.entry_cord), "Error",Snackbar.LENGTH_LONG );
                    mySnackbar.show();}

                Toast.makeText(getActivity(),"Upload Failed",Toast.LENGTH_LONG);

                one = (EditText) v.findViewById(R.id.editText);
                MobileNo = (EditText) v.findViewById(R.id.CellNo);
                one.setText("");
                MobileNo.setText("");

                //ADDING "E" TO ENTRIES WITH ERROR
                d.entryStatus(veh);
            }
        }){

            protected Map<String,String> getParams(){

                Map<String,String> params = new HashMap<String,String>();

                params.put("imei", device_id);
                params.put("entry",finalDate);
                params.put("mobileno",cell);
                params.put("type",type);
                params.put("vehicleno",veh);
                return params;

            }
        };
// Add the request to the RequestQueue.
        mRequestQueue.add(stringRequest);
    }


    //EXPORT TO EXCEL
    public void exportToExcel(Cursor cursor) {
        final String fileName = "VehicleList.xls";

        //Saving file in external storage
        File sdCard = Environment.getExternalStorageDirectory();

        File directory = new File(sdCard.getAbsolutePath() + "/ParkingLotFolder");
        // Log.i(TAG, "Path Created");

        //create directory if not exist
        if(!directory.isDirectory()){
            directory.mkdirs();
        }

        //file path
        File file = new File(directory, fileName);

        WorkbookSettings wbSettings = new WorkbookSettings();
        wbSettings.setLocale(new Locale("en", "EN"));

        WritableWorkbook workbook;

        try {
            workbook = Workbook.createWorkbook(file, wbSettings);

            //Excel sheet name. 0 represents first sheet
            WritableSheet sheet = workbook.createSheet("VehicleData", 0);

            try {
                sheet.addCell(new Label(0, 0, "ID")); // column and row
                sheet.addCell(new Label(1, 0, "VEHICLE NO."));
                sheet.addCell(new Label(2, 0, "TYPE"));
                sheet.addCell(new Label(3, 0, "MOB NO."));
                sheet.addCell(new Label(4, 0, "ENTRY"));
                sheet.addCell(new Label(5, 0, "EXIT"));
                //sheet.addCell(new Label(4, 0, "ADDRESS"));

                if (cursor.moveToFirst()) {
                    do {
                        String id = cursor.getString(cursor.getColumnIndex(d.COLUMN_ID));
                        String name = cursor.getString(cursor.getColumnIndex(d.COLUMN_NAME));
                        String phone = cursor.getString(cursor.getColumnIndex(d.COLUMN_CELL));
                        String type = cursor.getString(cursor.getColumnIndex(d.COLUMN_TYPE));
                        String entry    = cursor.getString(cursor.getColumnIndex(d.COLUMN_ENTRY));
                        String exit = cursor.getString(cursor.getColumnIndex(d.COLUMN_EXIT));
                        //String address = cursor.getString(cursor.getColumnIndex(d.COLUMN_ADDRESS));

                        int i = cursor.getPosition() + 1;

                        sheet.addCell(new Label(0, i, id));
                        sheet.addCell(new Label(1, i, name));
                        sheet.addCell(new Label(2, i, type));
                        sheet.addCell(new Label(3, i, phone));
                        sheet.addCell(new Label(4, i, entry));
                        sheet.addCell(new Label(5, i, exit));
                        //sheet.addCell(new Label(4, i, address));

                    } while (cursor.moveToNext());
                }

                //closing cursor
                cursor.close();

            } catch (RowsExceededException e) {
                e.printStackTrace();
            } catch (WriteException e) {
                e.printStackTrace();
            }

            workbook.write();

            try {
                workbook.close();
            } catch (WriteException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
