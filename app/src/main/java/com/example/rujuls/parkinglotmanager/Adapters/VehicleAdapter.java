package com.example.rujuls.parkinglotmanager.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rujuls.parkinglotmanager.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RUJUL S on 08-10-2016.
 */
public class VehicleAdapter extends ArrayAdapter implements Filterable{

    public List list = new ArrayList();
    List updateList = new ArrayList();
    CustomFilter filter;
    VehicleInfo vehicleInfo;

    static class LayoutHandler{
        TextView VEHICLE;
        TextView TIME;
    }
    public VehicleAdapter(Context context, int resource) {
        super(context, resource);
    }


    public void add(VehicleInfo object) {
        list.add(object);
        updateList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);

    }
    public void remove(int i){
        list.remove(list.get(i));
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;

        LayoutHandler layoutHandler;

        if(row==null){

            LayoutInflater layoutInflater =  (LayoutInflater) this.getContext().getSystemService(getContext().LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.fragment_vehicle_row,parent,false);
            layoutHandler = new LayoutHandler();
            layoutHandler.VEHICLE = (TextView) row.findViewById(R.id.veh_name);
            layoutHandler.TIME= (TextView) row.findViewById(R.id.veh_no);
            row.setTag(layoutHandler);
        }
        else{
            layoutHandler = (LayoutHandler) row.getTag();
        }

        vehicleInfo = (VehicleInfo)this.getItem(position);
        layoutHandler.VEHICLE.setText(vehicleInfo.getVeh());
        layoutHandler.TIME.setText(vehicleInfo.getTime());

        return row;
    }

    public Filter getFilter(){

        if (filter == null ){

            filter = new CustomFilter();
        }

        return filter;

    }

    class CustomFilter extends Filter{
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if(constraint!=null && constraint.length()>0){
                constraint=constraint.toString().toUpperCase();

               List filters = new ArrayList();

                for(int i =0;i<updateList.size();i++){

                    if(updateList.get(i).toString().toUpperCase().contains(constraint)){

                        Toast.makeText(getContext(),filters.get(i).getClass().getName(), Toast.LENGTH_SHORT).show();
                        VehicleInfo vehicleInfo = new VehicleInfo(filters.get(i).toString());
                       filters.add(vehicleInfo);
                    }
                }
                results.count = filters.size();
                results.values = filters;
            }else{
                results.count = updateList.size();
                results.values = updateList;
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            list = (List) results.values;
            notifyDataSetChanged();

        }
    }




}
