package com.example.rujuls.parkinglotmanager.Pass;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.rujuls.parkinglotmanager.Database.DBHandler;
import com.example.rujuls.parkinglotmanager.Database.PassStore;
import com.example.rujuls.parkinglotmanager.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PassChoiceActivity extends AppCompatActivity {

    String unique;
    EditText e1;
    EditText e2;
    static String veh;
    static String mob;
    TextView t;
    String device_id;
    DBHandler d;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_choice);
        d = new DBHandler(getApplicationContext());
        unique = getIntent().getStringExtra(PassDetailsActivity.PASS_UNI);
        e1 = (EditText) findViewById(R.id.editText5);
        e2 = (EditText) findViewById(R.id.editText7);

    }

    public void onDone(View view){
        veh = e1.getText().toString();
        mob = e2.getText().toString();
        if(mob.matches("")|| mob.length()!=10){

            if(veh.matches("")){Toast.makeText(this,"Invalid Entries",Toast.LENGTH_SHORT).show();}
            else{ Toast.makeText(this,"Invalid Mobile No",Toast.LENGTH_SHORT).show();}
        }
        else if(veh.matches("")){Toast.makeText(this,"Invalid Vehicle NO",Toast.LENGTH_SHORT).show();}
        else{
        postString("https://api.plvm.in/pass/new");}
    }

    void postString(String url){

         t = (TextView) findViewById(R.id.status);
        TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        device_id = tm.getDeviceId();
        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //t.setText("Response is: "+ response);
                        if(response.matches("Success")){
                            t.setText("Pass Added Successfully");
                            d.passDone(unique,veh,mob);
                            e1.setText("");
                            e2.setText("");
                        }else{t.setText(response);}

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                t.setText("Please Check Your Connection");
            }
        }){

            protected Map<String,String> getParams(){

                Map<String,String> params = new HashMap<String,String>();

                params.put("imei", device_id);
                params.put("vehicleno", veh);
                params.put("uniqueno", unique);
                params.put("mobileno", mob);
                return params;

            }
        };
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
    }

