package com.example.rujuls.parkinglotmanager.Adapters;

/**
 * Created by RUJUL S on 26-10-2016.
 */

public class passInfo {

    private String pass;
    private String price;
    private  String hidden;

    public passInfo(String pass, String price, String hidden){
        this.pass = pass;
        this.price = price;
        this.hidden = hidden;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPrice() {
        return price;
    }

    public String getHidden() {
        return hidden;
    }

    public void setHidden(String hidden) {
        this.hidden = hidden;
    }
}
