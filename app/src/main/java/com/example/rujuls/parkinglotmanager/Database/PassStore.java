package com.example.rujuls.parkinglotmanager.Database;

/**
 * Created by RUJUL S on 26-10-2016.
 */

public class PassStore {

    private int _id;
    private String _pass;
    private String no;
    private String _dur;
    private String _price;
    private String _type;
    private long _cell;

    public PassStore(String pass, String no, String dur, String price, String type) {
        this._pass = pass;
        this.no = no;
        this._dur = dur;
        //this._pass = pass;
        this._type = type;
        //this._cell = cell;
        this._price = price;
    }


    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_pass() {
        return _pass;
    }

    public void set_pass(String _pass) {
        this._pass = _pass;
    }

    public String get_no() {
        return no;
    }

    public void setDetails(String details) {
        this.no = details;
    }

    public long get_cell() {
        return _cell;
    }

    public void set_cell(long _cell) {
        this._cell = _cell;
    }

    public String get_dur() {
        return _dur;
    }

    public void set_dur(String _dur) {
        this._dur = _dur;
    }

    public String get_price() {
        return _price;
    }

    public void set_price(String _price) {
        this._price = _price;
    }

    public String get_type() {
        return _type;
    }

    public void set_type(String _type) {
        this._type = _type;
    }
}
