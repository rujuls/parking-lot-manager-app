package com.example.rujuls.parkinglotmanager.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.rujuls.parkinglotmanager.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RUJUL S on 26-10-2016.
 */

public class PassAdapter extends ArrayAdapter{

    List list = new ArrayList();
    List updateList = new ArrayList();
    //PassAdapter.CustomFilter filter;
    com.example.rujuls.parkinglotmanager.Adapters.passInfo passInfo;

    static class LayoutHandler{
        TextView PASS;
        TextView PRICE;
        TextView HIDDEN;
    }
    public PassAdapter(Context context, int resource) {
        super(context, resource);
    }


    public void add(passInfo object) {
        list.add(object);
        updateList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;

        PassAdapter.LayoutHandler layoutHandler;

        if(row==null){

            LayoutInflater layoutInflater =  (LayoutInflater) this.getContext().getSystemService(getContext().LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.pass_row,parent,false);
            layoutHandler = new PassAdapter.LayoutHandler();
            layoutHandler.PASS = (TextView) row.findViewById(R.id.pass_name);
            layoutHandler.PRICE = (TextView) row.findViewById(R.id.pass_no);
            layoutHandler.HIDDEN = (TextView) row.findViewById(R.id.hidden);
            row.setTag(layoutHandler);
        }
        else{
            layoutHandler = (PassAdapter.LayoutHandler) row.getTag();
        }

        passInfo = (passInfo)this.getItem(position);
        layoutHandler.PASS.setText(passInfo.getPass());
        layoutHandler.PRICE.setText(passInfo.getPrice());
        layoutHandler.HIDDEN.setText(passInfo.getHidden());

        return row;
    }
}
