package com.example.rujuls.parkinglotmanager.Main;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;


import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.rujuls.parkinglotmanager.Adapters.VehicleAdapter;
import com.example.rujuls.parkinglotmanager.Database.DBHandler;
import com.example.rujuls.parkinglotmanager.Database.DataStore;
import com.example.rujuls.parkinglotmanager.Adapters.VehicleInfo;
import com.example.rujuls.parkinglotmanager.Exit.ExitDetails;
import com.example.rujuls.parkinglotmanager.R;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ExitFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


   private static final String TAG = "com.example.rujuls.parkinglotmanager;";
    public static final String VEH_EXTRA ="com.example.rujuls.parkinglotmanager._VEH";
    public static final String POS_EXTRA ="com.example.rujuls.parkinglotmanager._POS";




    static String device_id;
    static String finalTime;

    DBHandler d;
    static VehicleAdapter vehicleAdapter;
    ListView listView;
    SearchView search;
    public static final String COLUMN_NAME = "VEHICLE";
    public static final String TABLE_PRODUCTS = "VehicleDataSheet";
    private SwipeRefreshLayout swipe;

    TextView t;
    public static ExitFragment newInstance() {
        // Required empty public constructor
        ExitFragment exit = new ExitFragment();
        return exit;
    }

    public ExitFragment(){

    }

    View v;
    Button b;
    Tracker mTracker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_vehicle_list, container, false);

        //String[] food = {"Pizza", "Burger","Salads","Sandwich","Hot-Dogs","Fries"};

        //ListAdapter foodAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,food);/listView = (ListView)  v.findViewById(R.id.display_list);
        //listView.setAdapter(foodAdapter);
        setHasOptionsMenu(true);
        d = new DBHandler(getActivity().getApplicationContext());

        AnalyticsApp application = (AnalyticsApp) getActivity().getApplication();
        mTracker = application.getDefaultTracker();

            //Toast.makeText(getActivity(),""+c.getCount(),Toast.LENGTH_LONG).show();

       //ListSetup(d);
        listView = (ListView) v.findViewById(R.id.display_list);

        swipe = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);

        swipe.setOnRefreshListener(this);
        vehicleAdapter = new VehicleAdapter(getActivity().getApplicationContext(), R.layout.fragment_vehicle_row);

       // ListSetup(d);

        search = (SearchView) v.findViewById(R.id.searchView2);

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String search) {
                d = new DBHandler(getActivity().getApplicationContext());
                SearchList( d,  search);

                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                //vehicleAdapter = new VehicleAdapter(getActivity().getApplicationContext(), R.layout.fragment_vehicle_row);
                //vehicleAdapter.getFilter().filter(s)
                return true;
            }
        });

        swipe.post(new Runnable() {
                       @Override
                       public void run() {
                           swipe.setRefreshing(true);
                           getString("https://api.plvm.in/vehicle/list");
                           d = new DBHandler(getActivity().getApplicationContext());
                           ListSetup(d);
                       }
                   }
        );
                    /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        swipe.setRefreshing(true);
        getString("https://api.plvm.in/vehicle/list");
        d = new DBHandler(getActivity().getApplicationContext());
        ListSetup(d);
        vehicleAdapter.notifyDataSetChanged();
        mTracker.setScreenName("Exit Fragment");

// Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public void SearchList(DBHandler d, String search){


        SQLiteDatabase db = d.getWritableDatabase();
        String query = ("SELECT * FROM " + TABLE_PRODUCTS+ " WHERE " +COLUMN_NAME+ "=\"" +search+ "\"" );
        Cursor cursor = db.rawQuery(query, null);

        vehicleAdapter = new VehicleAdapter(getActivity().getApplicationContext(), R.layout.fragment_vehicle_row);

        //Cursor cursor = d.exitFetchList();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            String entry = cursor.getString(cursor.getColumnIndex(d.COLUMN_NAME));
            String time = cursor.getString(cursor.getColumnIndex(d.COLUMN_ENTRY));

            VehicleInfo vehicleInfo = new VehicleInfo(entry,time);
            vehicleAdapter.add(vehicleInfo);
            //cal = doWork(entry);
            cursor.moveToNext();
        }

        // VehList(v);
        vehicleAdapter.notifyDataSetChanged();
        listView.setAdapter(vehicleAdapter);


    }

    public void ListSetup(DBHandler d){
        swipe.setRefreshing(true);

        SQLiteDatabase db = d.getWritableDatabase();
        String query = ("SELECT * FROM " + TABLE_PRODUCTS );
        Cursor cursor = db.rawQuery(query, null);
        //Cursor cursor = d.exitFetchList(db);

       // search = (SearchView) v.findViewById(R.id.searchView2);
        vehicleAdapter = new VehicleAdapter(getActivity().getApplicationContext(), R.layout.fragment_vehicle_row);

        //Cursor cursor = d.exitFetchList();
            cursor.moveToFirst();
                while (!cursor.isAfterLast()){
                    String entry = cursor.getString(cursor.getColumnIndex(d.COLUMN_NAME));
                    String time = cursor.getString(cursor.getColumnIndex(d.COLUMN_ENTRY));

                        VehicleInfo vehicleInfo = new VehicleInfo(entry,time);
                        vehicleAdapter.add(vehicleInfo);
                    //cal = doWork(entry);
                    cursor.moveToNext();
                }

            // VehList(v);
            vehicleAdapter.notifyDataSetChanged();
            listView.setAdapter(vehicleAdapter);
        swipe.setRefreshing(false);
        int t = vehicleAdapter.getCount();
       // Toast.makeText(getActivity(), "Count = "+t, Toast.LENGTH_SHORT).show();

        Cursor c = d.fetch();
       // MainActivity m = new MainActivity();
       // m.exportToExcel(c);

        listView.setOnItemClickListener(onListClick);
    }

    //GETTING VEHICLE DATA FROM SERVER
    void getString(String url){

        swipe.setRefreshing(true);
        TelephonyManager tm = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        device_id = tm.getDeviceId();
        final TextView t ;
        //t = (TextView) v.findViewById(R.id.textView16);
        RequestQueue queue = Volley.newRequestQueue(getActivity());

        d = new DBHandler(getActivity().getApplicationContext());
        d.getWritableDatabase();

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //t.setText("Response is: "+ response);
                        String JsonFinal = response;
                        try {
                            JSONArray parentArray = new JSONArray(JsonFinal);
                            StringBuffer finalBuffer = new StringBuffer();
                            String vehName = null;
                            String date = null;
                            String type;
                            for (int i=0; i<parentArray.length(); i++) {
                                JSONObject finalObject = parentArray.getJSONObject(i);

                                vehName = finalObject.getString("vehicleno");
                                type = finalObject.getString("vehicle_type");
                                date = finalObject.getString("entrytime");
                                date = date.replaceAll("T"," ");
                                date = date.replaceAll("Z","");
                                date = date.replaceAll("-","/");
                                //String ty = String.valueOf(type);
                                //ty.replaceAll("-bike","");

                                int c = d.addUpdatePro(vehName);
                                int e = d.addErrorPro(vehName);
                                //finalBuffer.append(vehName + "-" + date + "\n" + c + "");
                                finalBuffer.append( c + "");

                                /*String datetime[] = date.split("");
                                String time = datetime[1];
                                SimpleDateFormat
                                        _24HourSDF = new SimpleDateFormat("HH:mm:ss");
                                SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm:ss a");
                                Date _24HourDt = _24HourSDF.parse(time);
                                date = datetime[0] + _24HourDt; */
                               String[] newDate = date.split("\\s+");
                                SimpleDateFormat _24HourSDF = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
                                SimpleDateFormat _12HourSDF = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
                               // Date _24HourDt = _24HourSDF.parse(newDate[1]);
                                Date _12Time = _24HourSDF.parse(date);
                               finalTime = _12HourSDF.format(_12Time);

                                //TextView t = (TextView) v.findViewById(R.id.textView13);
                                //t.setText(finalTime);
                                //Toast.makeText(this,newDate[0], Toast.LENGTH_LONG).show();
                                //Log.i(TAG,newDate[0]);
                                if(c==0 && e==0) {
                                    DataStore f = new DataStore(vehName, finalTime, type);
                                    d.addUpdateProduct(f);
                                }
                            }
                            vehicleAdapter.notifyDataSetChanged();
                            swipe.setRefreshing(false);
                            //send data to postExecute
                            //t.setText(finalBuffer.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                            swipe.setRefreshing(false);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                       Cursor c = d.fetch();
                        if(c.getCount()!=0){
                            do{
                                int j =0;
                                String entry    = c.getString(c.getColumnIndex(d.COLUMN_NAME));
                                JSONArray parentArray = null;
                                try {
                                    parentArray = new JSONArray(JsonFinal);
                                    String vehName;
                                    for (int i=0; i<parentArray.length(); i++) {
                                        JSONObject finalObject = parentArray.getJSONObject(i);
                                        vehName = finalObject.getString("vehicleno");
                                      //  Toast.makeText(getActivity(),vehName,Toast.LENGTH_LONG).show();
                                        if(entry.equals(vehName)){
                                            j=1;
                                        }
                                        //Toast.makeText(getActivity(),""+j,Toast.LENGTH_LONG).show();
                                    }
                                    //Toast.makeText(getActivity(),"FINAL VALUE: "+j,Toast.LENGTH_LONG).show();
                                    if(j==0){
                                        d.deleteOneProduct(entry);
                                    //Toast.makeText(getActivity(),""+j,Toast.LENGTH_LONG).show();
                                    }
                                    vehicleAdapter.notifyDataSetChanged();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }while(c.moveToNext());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                swipe.setRefreshing(false);
               // t.setText("That didn't work!");
            }
        }){

            protected Map<String,String> getParams(){

                Map<String,String> params = new HashMap<String,String>();

                params.put("imei", device_id);
                return params;

            }
        };
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }



    private AdapterView.OnItemClickListener onListClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            //DATA SHOWING
            ViewGroup vg = (ViewGroup) view;
            TextView t = (TextView) vg.findViewById(R.id.veh_name);
            //Toast.makeText(getActivity(),t.getText().toString(),Toast.LENGTH_SHORT).show();
            Cursor cursor = d.sendTypeCell(t.getText().toString());
            cursor.moveToFirst();
            String type;
            String cell;
            String entry;
            Intent intent = new Intent(getActivity(),ExitDetails.class);
            while (!cursor.isAfterLast()){
                 type = cursor.getString(cursor.getColumnIndex(d.COLUMN_TYPE));
                 cell = cursor.getString(cursor.getColumnIndex(d.COLUMN_CELL));
                entry = cursor.getString(cursor.getColumnIndex(d.COLUMN_ENTRY));
                //cal = doWork(entry);
                intent.putExtra("CELL",cell);
                intent.putExtra("TYPE",type);
                intent.putExtra("ENTRY",entry);
                cursor.moveToNext();
            }

            intent.putExtra(VEH_EXTRA,t.getText().toString());
            intent.putExtra("YO",i);
            startActivity (intent);

            vehicleAdapter.notifyDataSetChanged();

        }
    };




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onRefresh() {
        getString("https://api.plvm.in/vehicle/list");
        d = new DBHandler(getActivity().getApplicationContext());
        ListSetup(d);
    }
}
