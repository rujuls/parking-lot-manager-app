package com.example.rujuls.parkinglotmanager.Exit;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.view.ViewGroup.LayoutParams;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.rujuls.parkinglotmanager.Adapters.VehicleAdapter;
import com.example.rujuls.parkinglotmanager.Database.DBHandler;
import com.example.rujuls.parkinglotmanager.Database.DataStore;
import com.example.rujuls.parkinglotmanager.Main.AnalyticsApp;
import com.example.rujuls.parkinglotmanager.Main.ExitFragment;
import com.example.rujuls.parkinglotmanager.Main.MainActivity;
import com.example.rujuls.parkinglotmanager.R;
import com.google.android.gms.analytics.HitBuilders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class ExitDetails extends AppCompatActivity {

    static String passVeh;
    DBHandler d;
    static String device_id;
    static String finalDate;
    static String finalEntry;
    static String price;
    static String dur;
    private static String TAG = "com.example.rujuls.parkinglotmanager;";
    static String savedDate;
    static String savedType;

    TextView t3;
    TextView t4;
    TextView detail;
    TextView detail_time;
    int pos;
    static VehicleAdapter vehicleAdapter;
    private Context mContext;
    private Activity mActivity;
    private ProgressBar spinner;

    private RelativeLayout mRelativeLayout;
    private Button mButton;
    String type;
    String cell;
    String entry;
    String myDate;
    Date systemDate;
    static int ol;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;

    Thread workerThread;

    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;
    OutputStream mmOutputStream;
    InputStream mmInputStream;

    private PopupWindow mPopupWindow;

    UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exit_details);
        spinner = (ProgressBar) findViewById(R.id.pbHeaderProgress);
        spinner.setVisibility(View.GONE);

        mContext = getApplicationContext();


        // Get the activity
        mActivity = ExitDetails.this;

        // Get the widgets reference from XML layout
        mRelativeLayout = (RelativeLayout) findViewById(R.id.rl_custom_layout);
        //mButton = (Button) findViewById(R.id.btn);

        // Set a click listener for the text view

        // Initialize a new instance of LayoutInflater servic

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        t3 = (TextView) findViewById(R.id.durr);
        t4 = (TextView) findViewById(R.id.pricee);

        detail = (TextView) findViewById(R.id.exit_veh_no_entry);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        passVeh = getIntent().getStringExtra(ExitFragment.VEH_EXTRA);
        detail.setText(passVeh);
        pos = getIntent().getIntExtra("YO", 0);
        type = getIntent().getStringExtra("TYPE");
        entry = getIntent().getStringExtra("ENTRY");
        cell = getIntent().getStringExtra("CELL");



        //Toast.makeText(this, ""+pos, Toast.LENGTH_SHORT).show();

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        systemDate = Calendar.getInstance().getTime();
        myDate = sdf.format(systemDate);
        detail_time = (TextView) findViewById(R.id.exit_detail_entry_time);
        detail_time.setText(myDate);
        vehicleAdapter = new VehicleAdapter(getApplicationContext(), R.layout.fragment_vehicle_row);
        findBT();


    }

    // this will find a bluetooth printer device
    void findBT() {
        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if(mBluetoothAdapter == null) {

            }
            if(!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBluetooth, 0);
            }
            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            if(pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    if (device.getName().equals("BlueTooth Printer")) {
                        mmDevice = device;
                        break;
                    }
                }
            }
            // Toast.makeText(this, "Bluetooth device found.", Toast.LENGTH_LONG);
            //myLabel.setText("Bluetooth device found.");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    void openBT() throws IOException {
        try {
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            if (!mmSocket.isConnected())
                mmSocket.connect();
            mmOutputStream = mmSocket.getOutputStream();
            mmInputStream = mmSocket.getInputStream();
            beginListenForData();
            Toast.makeText(this,"Bluetooth Opened",Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void beginListenForData() {
        try {
            final Handler handler = new Handler();
            // this is the ASCII code for a newline character
            final byte delimiter = 10;
            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];
            workerThread = new Thread(new Runnable() {
                public void run() {
                    while (!Thread.currentThread().isInterrupted() && !stopWorker) {
                        try {
                            int bytesAvailable = mmInputStream.available();
                            if (bytesAvailable > 0) {
                                byte[] packetBytes = new byte[bytesAvailable];
                                mmInputStream.read(packetBytes);
                                for (int i = 0; i < bytesAvailable; i++) {
                                    byte b = packetBytes[i];
                                    if (b == delimiter) {
                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length
                                        );
                                        // specify US-ASCII encoding
                                        final String data = new String(encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;
                                        // tell the user data were sent to bluetooth printer device
                                        handler.post(new Runnable() {
                                            public void run() {
//                                                myLabel.setText(data);
                                            }
                                        });
                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }
                        } catch (IOException ex) {
                            stopWorker = true;
                        }
                    }
                }
            });
            workerThread.start();
            //printData();
            Toast.makeText(this,"BEGIN LISTENING",Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public int getTimeRemaining() {

        long dateEvent = System.currentTimeMillis() - (127 * 24 * 3600 * 1000);
        Calendar sDate = toCalendar(dateEvent);
        Calendar eDate = toCalendar(System.currentTimeMillis());

        // Get the represented date in milliseconds
        long milis1 = sDate.getTimeInMillis();
        long milis2 = eDate.getTimeInMillis();

        // Calculate difference in milliseconds
        long diff = Math.abs(milis2 - milis1);
        long Diff = sDate.getTimeInMillis() - eDate.getTimeInMillis();

        //return (int)(diff / (24 * 60 * 60 * 1000));
        return (int) (Diff / (24 * 60 * 60 * 1000));
    }

    private Calendar toCalendar(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    //CALCULATION ALGO
    public String[] doWork(String entry, String type) {
        savedDate = entry;
        savedType = type;
        final String[] cal = new String[2];
        //final String[] dur = new String[1];
        runOnUiThread(new Runnable() {
            public void run() {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
                    Date systemDate = Calendar.getInstance().getTime();
                    String myDate = sdf.format(systemDate);
//                  txtCurrentTime.setText(myDate);
                    // TextView t = (TextView) findViewById(R.id.textView16);
                    //t.setText(finalTime);

                    Date Date1 = sdf.parse(myDate);

                    Date Date2 = sdf.parse(savedDate);

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss aa");

                    Date date1 = simpleDateFormat.parse("10/10/2013 11:30:10 am");
                    Date date2 = simpleDateFormat.parse("13/10/2013 8:35:55 pm");


                    long different = Date1.getTime() - Date2.getTime();

                    long differ = different / (1000 * 60);

                    long hr = different / (1000 * 60 * 60);

                    long secondsInMilli = 1000;
                    long minutesInMilli = secondsInMilli * 60;
                    long hoursInMilli = minutesInMilli * 60;
                    long daysInMilli = hoursInMilli * 24;

                    long elapsedDays = different / daysInMilli;
                    different = different % daysInMilli;

                    long elapsedHours = different / hoursInMilli;
                    different = different % hoursInMilli;

                    long elapsedMinutes = different / minutesInMilli;
                    different = different % minutesInMilli;

                    long elapsedSeconds = different / secondsInMilli;


                    //long Mins = mills/(1000*60);
                    //String diff = Hours + ":" + Mins + ":" + Secs; // updated value every1 second
                    String diff;
                    if (differ >= 1440) {
                        diff = +elapsedDays + " Days " + elapsedHours + " Hours ";
                    } else {
                        diff = +elapsedHours + " Hours " + elapsedMinutes + " Mins";
                    }
                    cal[1] = "" + differ;
                    t3.setText(diff);

                    long rate;


                    if (savedType.matches("2-bike")) {
                        long x = 1;
                        long i = 0;
                                if (hr < 3) {
                                    rate = 20;
                                    cal[0] = "" + rate;
                                } else {
                                    rate = 20;
                                    for (i = hr; i >= 3; i--) {
                                        rate = rate + 10;
                                        cal[0] = "" + rate;
                                    }
                                }

                    } else {
                        long x = 1;
                        long i = 0;
                                if (hr < 3) {
                                    rate = 30;
                                    cal[0] = "" + rate;
                                } else {
                                    rate = 30;
                                    for (i = hr; i >= 3; i--) {
                                        rate = rate + 10;
                                        cal[0] = "" + rate;
                                    }
                                }
                    }
                } catch (Exception e) {

                }
            }
        });
        return (cal);
    }


    public void onCheckout(View v) {

        checkForPass();
        spinner.setVisibility(View.VISIBLE);

    }


    public void checkForPass() {
        postString("https://api.plvm.in/pass/check");
    }

    void postString(String url) {

        //t = (TextView) findViewById(R.id.status);
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        device_id = tm.getDeviceId();
        RequestQueue queue = Volley.newRequestQueue(this);


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //t.setText("Response is: "+ response);
                        spinner.setVisibility(View.GONE);

                        if (response.matches("Y")) {
                            onPass();
                            //status.setText("Pass Applied");
                            Toast.makeText(getApplicationContext(), "Pass Applied", Toast.LENGTH_LONG).show();
                        } else {
                            onNoPass();
                            //status.setText("No Pass");
                            Toast.makeText(getApplicationContext(), "No Pass", Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //status.setText("No Internet Connection");
                onNoPass();
                spinner.setVisibility(View.GONE);
                Snackbar mySnackbar = Snackbar.make(findViewById(R.id.myCoordinatorLayout), "No Internet Connection", Snackbar.LENGTH_LONG);
                mySnackbar.show();
            }
        }) {

            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                params.put("imei", device_id);
                params.put("vehicleno", passVeh);
                //params.put("uniqueno", unique);
                //params.put("mobileno", mob);
                return params;
            }
        };
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    public void onPass() {

        SimpleDateFormat rd = new SimpleDateFormat("yyyy-MM-dd");

        //EXIT VEHICLE NO.
        //EditText e2 = (EditText) findViewById(R.id.editText2);
        //String ExitValue = e2.getText().toString();

        String[] cal;

        d = new DBHandler(getApplicationContext());

        Cursor cursor = d.exitFetch(passVeh);

        // t = (TextView) findViewById(R.id.textView3);

        //EXIT DURATION AND PRICE
        if (cursor.moveToFirst()) {
            do {
                String entry = cursor.getString(cursor.getColumnIndex(d.COLUMN_ENTRY));
                String type = cursor.getString(cursor.getColumnIndex(d.COLUMN_TYPE));
                cal = doWork(entry, type);
            } while (cursor.moveToNext());
            //EXIT TIME
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
            Date systemDate = Calendar.getInstance().getTime();
            String myDate = sdf.format(systemDate);
            String date = rd.format(systemDate);
            finalDate = date + " " + myDate;
            d.exitTime(passVeh, myDate, price, dur);
            price = "0";
            t4.setText(price);
            dur = cal[1];
            Cursor c = d.fetch();
            //MainActivity m = new MainActivity();
            // m.exportToExcel(c);
        }
    }

    public void onNoPass() {

        SimpleDateFormat rd = new SimpleDateFormat("yyyy-MM-dd");
        //EXIT VEHICLE NO.
        //EditText e2 = (EditText) findViewById(R.id.editText2);
        //String ExitValue = e2.getText().toString();
        String[] cal;

        d = new DBHandler(getApplicationContext());

        Cursor cursor = d.exitFetch(passVeh);
        // t = (TextView) findViewById(R.id.textView3);

        //EXIT DURATION AND PRICE
        if (cursor.moveToFirst()) {
            do {
                String entry = cursor.getString(cursor.getColumnIndex(d.COLUMN_ENTRY));
                String type = cursor.getString(cursor.getColumnIndex(d.COLUMN_TYPE));

                cal = doWork(entry, type);

            } while (cursor.moveToNext());

            //EXIT TIME
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
            Date systemDate = Calendar.getInstance().getTime();
            String myDate = sdf.format(systemDate);
            String date = rd.format(systemDate);

            finalDate = date + " " + myDate;


            price = cal[0];
            t4.setText(price);
            dur = cal[1];

            d.exitTime(passVeh, myDate, price, dur);
            Cursor c = d.fetch();
            //MainActivity m = new MainActivity();
            //m.exportToExcel(c);
        }
    }

    public void onExitDone(View view) {


        //getString("https://api.plvm.in/vehicle/list");
        //vehicleAdapter.remove(pos);
        Cursor cursor = d.exitFetch(passVeh);
        if (cursor.getCount() != 0) {
            do {
                String cell = cursor.getString(cursor.getColumnIndex(d.COLUMN_CELL));
                String type = cursor.getString(cursor.getColumnIndex(d.COLUMN_TYPE));
                String entry = cursor.getString(cursor.getColumnIndex(d.COLUMN_ENTRY));
                String finalTime;
                SimpleDateFormat _24HourSDF = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
                SimpleDateFormat _12HourSDF = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
                try {
                    finalTime = _24HourSDF.format(systemDate);
                    Date _12Entry = _12HourSDF.parse(entry);
                    finalEntry = _24HourSDF.format(_12Entry);
                    exitString("https://api.plvm.in/vehicle/exit", passVeh, cell, type, finalEntry, finalTime, price, dur);
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
            } while (cursor.moveToNext());

            try {
                openBT();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            //BLUETOOTH PRINTING CODE
            try {
                Toast.makeText(this,"PRINTTTTTT!!!!!!!!!!!!!!!!!",Toast.LENGTH_SHORT).show();

//                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
//                byte[] image = Utils.decodeBitmap(bitmap);
//                mmOutputStream.write(image);


                String titleStr = "\n          www.plvm.in\n";
                titleStr += "            RECEIPT\n";
                titleStr += "       I.P. MALL PAY & PARK\n";
                titleStr += "          BORIVALI (W)\n";
                titleStr += "********************************\n";
                titleStr += "       VEHICLE TYPE: " +type+ "\n";
                titleStr += "       VEHICLE NO : "+ passVeh +"\n";
                titleStr += "       FINAL DURATION : "+ dur +" mins\n";
                titleStr += "       FINAL AMOUNT: " +price+ "\n";
                titleStr += "********************************\n";
                titleStr +=    "THANK YOU.PLEASE VISIT AGAIN\n";

                mmOutputStream.write(titleStr.getBytes());

//                Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.advert);
//                byte[] image2 = Utils.decodeBitmap(bitmap1);
//                mmOutputStream.write(image2);
//
//                Bitmap bitmap2 = BitmapFactory.decodeResource(getResources(), R.drawable.advert2);
//                byte[] image3 = Utils.decodeBitmap(bitmap2);
//                mmOutputStream.write(image3);

                String titleStr1 = "\n\n\n";
                mmOutputStream.write(titleStr1.getBytes());
                mmOutputStream.write("".getBytes());
                mmOutputStream.write("".getBytes());

                Toast.makeText(this,"Printed Reciept",Toast.LENGTH_LONG).show();
            } catch (Exception error) {
                error.printStackTrace();
            }
            try {
                closeBT();
            } catch (IOException errr) {
                errr.printStackTrace();
            }
        }

        //RESENDING FAILED EXITS
        Runnable r = new Runnable() {
            @Override
            public void run() {
                Cursor c = d.sendErrorExit();
                if (c.getCount() != 0) {
                    do {
                        //Toast.makeText(this, "Method", Toast.LENGTH_SHORT).show();
                        Log.i("Error Send", "Get data");
                        String passVeh = c.getString(c.getColumnIndex(d.ERR_NAME));
                        String cell = c.getString(c.getColumnIndex(d.ERR_CELL));
                        String type = c.getString(c.getColumnIndex(d.ERR_TYPE));
                        String entry = c.getString(c.getColumnIndex(d.ERR_ENTRY));
                        String date = c.getString(c.getColumnIndex(d.ERR_EXIT));
                        String price = c.getString(c.getColumnIndex(d.ERR_PRICE));
                        String dur = c.getString(c.getColumnIndex(d.ERR_DUR));

                        Log.i("Error Send", "Link");
                        exitErrorString("https://api.plvm.in/vehicle/exit", passVeh, cell, type, entry, date, price, dur);
                    } while (c.moveToNext());
                }
            }
        };
        Thread thread = new Thread(r);
        thread.run();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                //Intent i =new Intent(ExitDetails.this,MainActivity.class);
                //startActivity(i);
                finish();
            }
        }, 2000);

    }

    // close the connection to bluetooth printer.
    void closeBT() throws IOException {
        try {
            stopWorker = true;
            mmOutputStream.close();
            mmInputStream.close();
            mmSocket.close();
            Toast.makeText(this,"BT Closed" ,Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //EXIT DATA TO SERVER
    void exitString(String url, final String veh, final String cell, final String type, final String finalEntry, final String finalExit, final String price, final String dur) {

        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        device_id = tm.getDeviceId();

        RequestQueue queue = Volley.newRequestQueue(this);

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.matches("Success")) {
                            d = new DBHandler(getApplicationContext());
                            d.deleteOneProductExit(veh);
                            d.deleteErrorProduct(veh);
                            Snackbar mySnackbar = Snackbar.make(findViewById(R.id.myCoordinatorLayout), "Upload Successfull", Snackbar.LENGTH_LONG);
                            mySnackbar.show();
                        }
                        Snackbar mySnackbar = Snackbar.make(findViewById(R.id.myCoordinatorLayout), response, Snackbar.LENGTH_LONG);
                        mySnackbar.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // status.setText("That didn't work!");
                Snackbar mySnackbar = Snackbar.make(findViewById(R.id.myCoordinatorLayout), "Upload Failed", Snackbar.LENGTH_LONG);
                mySnackbar.show();
                Toast.makeText(getApplication(),"Exit Complete",Toast.LENGTH_LONG).show();
                d.addErrorProduct(veh, cell, type, finalEntry, finalExit, dur, price);
                d = new DBHandler(getApplicationContext());
                d.deleteOneProductExit(veh);
            }
        }) {

            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                //params.put("username", e.getText().toString());
                params.put("imei", device_id);
                params.put("entry", finalEntry);
                if(cell!=null)params.put("mobileno", cell);
                params.put("type", type);
                params.put("vehicleno", veh);
                params.put("exit", finalExit);
                params.put("price", price);
                params.put("duration", dur);
                return params;

            }
        };
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    void exitErrorString(String url, final String veh, final String cell, final String type, final String finalEntry, final String finalExit, final String price, final String dur) {

        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        device_id = tm.getDeviceId();

        RequestQueue queue = Volley.newRequestQueue(this);

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //status.setText("Response is: "+ response);
                        if (response.matches("Success")) {
                            d = new DBHandler(getApplicationContext());
                            d.deleteOneProduct(veh);
                            d.deleteErrorProduct(veh);
                            Snackbar mySnackbar = Snackbar.make(findViewById(R.id.myCoordinatorLayout), "Upload Successfull", Snackbar.LENGTH_LONG);
                            mySnackbar.show();
                        }
                        Snackbar mySnackbar = Snackbar.make(findViewById(R.id.myCoordinatorLayout), response, Snackbar.LENGTH_LONG);
                        mySnackbar.show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // status.setText("That didn't work!");
                Snackbar mySnackbar = Snackbar.make(findViewById(R.id.myCoordinatorLayout), "Upload Failed", Snackbar.LENGTH_LONG);
                mySnackbar.show();
                //d.addErrorProduct(veh,cell,type,finalEntry,finalExit,dur,price);
                d = new DBHandler(getApplicationContext());
                d.deleteOneProduct(veh);
            }
        }) {

            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                //params.put("username", e.getText().toString());
                params.put("imei", device_id);
                params.put("entry", finalEntry);
                params.put("mobileno", cell);
                params.put("type", type);
                params.put("vehicleno", veh);
                params.put("exit", finalExit);
                params.put("price", price);
                params.put("duration", dur);
                return params;

            }
        };
// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


}