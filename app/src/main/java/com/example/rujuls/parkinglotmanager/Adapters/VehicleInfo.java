package com.example.rujuls.parkinglotmanager.Adapters;

import android.widget.Filterable;

/**
 * Created by RUJUL S on 08-10-2016.
 */
public class VehicleInfo {

    private String veh;
    private String time;

    public VehicleInfo(String veh,String time){

        this.setVeh(veh);
        this.setTime(time);

    }

    public VehicleInfo(String veh){

        this.setVeh(veh);

    }


    public String getVeh() {
        return veh;
    }

    public void setVeh(String veh) {
        this.veh = veh;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
