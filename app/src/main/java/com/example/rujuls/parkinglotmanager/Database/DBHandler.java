package com.example.rujuls.parkinglotmanager.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.rujuls.parkinglotmanager.Database.DataStore;

public class DBHandler extends SQLiteOpenHelper{

    private static String TAG = "com.example.rujuls.datafeed";
    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "VehicleData.db";
    public static final String TABLE_PRODUCTS = "VehicleDataSheet";
    public static final String TABLE_PASS = "PassDataSheet";
    public static final String TABLE_PASS_ASSIGN = "PassAssignSheet";

    public static final String TABLE_ERR = "ErrorDataSheet";

    public static final String COLUMN_ID = "ID";
    public static final String COLUMN_ID_PASS = "ID";
    public static final String COLUMN_NAME = "VEHICLE";
    public static final String COLUMN_PASS = "PASS";
    public static final String COLUMN_PASS_TYPE = "TYPE";
    public static final String COLUMN_PASS_PRICE = "PRICE";
    public static final String COLUMN_PASS_NO = "UNINO";
    public static final String COLUMN_PASS_DURATION = "DURATION";
    public static final String COLUMN_ENTRY = "ENTRY";
    public static final String COLUMN_CELL = "PHONE";
    public static final String COLUMN_CELL_PASS = "PHONE";
    public static final String COLUMN_EXIT = "EXIT";
    public static final String COLUMN_DUR = "DURATION";
    public static final String COLUMN_TYPE = "TYPE";
    public static final String COLUMN_PRICE = "PRICE";
    public static final String COLUMN_ENTRY_STATUS = "EntrySTATUS";
    public static final String COLUMN_EXIT_STATUS = "ExitSTATUS";

    public static final String ERR_ID = "ID";
    public static final String ERR_NAME = "VEHICLE";
    public static final String ERR_TYPE = "TYPE";
    public static final String ERR_CELL = "PHONE";
    public static final String ERR_ENTRY = "ENTRY";
    public static final String ERR_EXIT = "EXIT";
    public static final String ERR_DUR = "DURATION";
    public static final String ERR_PRICE = "PRICE";


    private static final String query = "CREATE TABLE " + TABLE_PRODUCTS + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_NAME + " TEXT, " + COLUMN_TYPE + " TEXT, " + COLUMN_CELL + " TEXT, " + COLUMN_ENTRY + " TEXT, " +
            COLUMN_EXIT + " TEXT, "  + COLUMN_DUR + " TEXT, "  + COLUMN_PRICE + " TEXT, "  + COLUMN_ENTRY_STATUS + " TEXT, " + COLUMN_EXIT_STATUS + " TEXT " +
            ");";

    private static final String errorQuery = "CREATE TABLE " + TABLE_ERR + "(" + ERR_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
           ERR_NAME + " TEXT, " + ERR_TYPE + " TEXT, " + ERR_CELL + " TEXT, " + ERR_ENTRY + " TEXT, " +
            ERR_EXIT + " TEXT, "  + ERR_DUR + " TEXT, "  + ERR_PRICE + " TEXT " +
            ");";

    private static final String passQuery = "CREATE TABLE " + TABLE_PASS + "(" + COLUMN_ID_PASS + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_PASS + " TEXT, " + COLUMN_PASS_NO + " TEXT, " + COLUMN_PASS_TYPE + " TEXT, " + COLUMN_PASS_DURATION + " TEXT, " + COLUMN_PASS_PRICE + " TEXT, " + COLUMN_NAME + " TEXT, " + COLUMN_CELL_PASS + " TEXT " +
            ");";

    public DBHandler(Context context) {
        super(context, "/mnt/sdcard/"+DATABASE_NAME+".db", null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(query);
        sqLiteDatabase.execSQL(errorQuery);
        sqLiteDatabase.execSQL(passQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PASS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ERR);
        onCreate(db);

    }


    public void addProduct(DataStore f){
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, f.get_veh());
        values.put(COLUMN_TYPE,f.get_type());
        values.put(COLUMN_CELL,f.get_cell());
        values.put(COLUMN_ENTRY, f.getEntry_time());
        values.put(COLUMN_EXIT, f.getExit_time());
        //values.put(COLUMN_ADDRESS, f.get_address());
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_PRODUCTS, null, values);
        db.close();
    }

    public void addPassProduct(PassStore p){
        ContentValues values = new ContentValues();
        values.put(COLUMN_PASS, p.get_pass());
        values.put(COLUMN_PASS_NO, p.get_no());
        values.put(COLUMN_PASS_TYPE, p.get_type());
        values.put(COLUMN_PASS_PRICE, p.get_price());
        values.put(COLUMN_PASS_DURATION,p.get_dur());
        //values.put(COLUMN_CELL,p.get_cell());
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_PASS, null, values);
        db.close();
    }

    public void addErrorProduct(String name, String cell, String type, String entry, String exit, String dur, String price){
        ContentValues values = new ContentValues();
        values.put(ERR_NAME, name);
        values.put(ERR_TYPE,type);
        values.put(ERR_CELL,cell);
        values.put(ERR_ENTRY, entry);
        values.put(ERR_EXIT, exit);
        values.put(ERR_DUR, dur);
        values.put(ERR_PRICE, price);

        //values.put(COLUMN_ADDRESS, f.get_address());
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_ERR, null, values);
        db.close();
    }


    public void addUpdateProduct(DataStore f){
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, f.get_veh());
        values.put(COLUMN_TYPE,f.get_type());
        //values.put(COLUMN_CELL,f.get_cell());
        values.put(COLUMN_ENTRY, f.getEntry_time());
        //values.put(COLUMN_EXIT, f.getExit_time());
        //values.put(COLUMN_ADDRESS, f.get_address());
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_PRODUCTS, null, values);
        db.close();
    }

    public Cursor fetch(){

        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE 1";
        Cursor recordSet = db.rawQuery(query, null);
        if (recordSet != null) {
            recordSet.moveToFirst();
        }
        return recordSet;

    }

    public Cursor exitFetch(String veh ){

        SQLiteDatabase db = getWritableDatabase();
        String query = ("SELECT * FROM " + TABLE_PRODUCTS + " WHERE " +COLUMN_NAME+ "=\"" +veh+ "\";");
          Cursor recordSet = db.rawQuery(query, null);
        if (recordSet != null) {
            recordSet.moveToFirst();
        }
        return recordSet;

    }

    public Cursor exitFetchList( SQLiteDatabase db ){

        //SQLiteDatabase db = getWritableDatabase();
        String query = ("SELECT " +COLUMN_ENTRY+ " FROM " + TABLE_PRODUCTS );
        Cursor recordSet = db.rawQuery(query, null);
        if (recordSet != null) {
            recordSet.moveToFirst();
        }
        return recordSet;

    }

    public int addUpdatePro(String veh){

        SQLiteDatabase db = getWritableDatabase();
        String query = ("SELECT " +COLUMN_ENTRY+ " FROM " + TABLE_PRODUCTS+ " WHERE " +COLUMN_NAME+  "=\"" +veh+ "\";" );
        Cursor recordSet = db.rawQuery(query, null);
       int i = recordSet.getCount();

        return i;

    }
    public int addErrorPro(String veh){

        SQLiteDatabase db = getWritableDatabase();
        String query = ("SELECT * FROM " + TABLE_ERR+ " WHERE " +ERR_NAME+  "=\"" +veh+ "\";" );
        Cursor recordSet = db.rawQuery(query, null);
        int i = recordSet.getCount();

        return i;

    }

    public int addErrorEntries(String veh){

        SQLiteDatabase db = getWritableDatabase();
        String query = ("SELECT " +COLUMN_ENTRY+ " FROM " + TABLE_ERR+ " WHERE " +COLUMN_NAME+  "=\"" +veh+ "\";" );
        Cursor recordSet = db.rawQuery(query, null);
        int i = recordSet.getCount();

        return i;

    }


    public int addUpdatePass(String veh){

        SQLiteDatabase db = getWritableDatabase();
        String query = ("SELECT " +COLUMN_PASS_NO+ " FROM " + TABLE_PASS+ " WHERE " +COLUMN_PASS_NO+  "=\"" +veh+ "\";" );
        Cursor recordSet = db.rawQuery(query, null);
        int i = recordSet.getCount();
        return i;

    }


    public void exitTime(String veh, String exit, String price, String dur) {

        //String veh = d.get_veh();
        //String exit = d.getExit_time();
        SQLiteDatabase db = getWritableDatabase();
        String query = " UPDATE " + TABLE_PRODUCTS+ " SET " + COLUMN_EXIT + "=\"" + exit + "\"," + COLUMN_PRICE + "=\"" + price + "\"," + COLUMN_DUR + "=\"" + dur + "\" WHERE " + COLUMN_NAME + "=\"" + veh + "\";";
        db.execSQL(query);

    }

    //ADDING E TO ERROR ENTRIES
    public void entryStatus(String veh) {

        //String veh = d.get_veh();
        //String exit = d.getExit_time();
        SQLiteDatabase db = getWritableDatabase();
        String exit = "E";
        String query = " UPDATE " + TABLE_PRODUCTS+ " SET " + COLUMN_ENTRY_STATUS + "=\"" + exit + "\" WHERE " + COLUMN_NAME + "=\"" + veh + "\";";
        db.execSQL(query);

    }

    //SELECTING E ERROR ENTRIES
    public Cursor sendErrorEntry(){

        SQLiteDatabase db = getWritableDatabase();
        String veh = "E";
        String query = ("SELECT * FROM " + TABLE_PRODUCTS+ " WHERE " +COLUMN_ENTRY_STATUS+  "=\"" +veh+ "\";" );
        Cursor recordSet = db.rawQuery(query, null);
        if (recordSet != null) {
            recordSet.moveToFirst();
        }
        return recordSet;
    }

    //REMOVING E FROM ERROR ENTRIES
    public void updateEntryStatus(String veh) {

        //String veh = d.get_veh();
        //String exit = d.getExit_time();
        SQLiteDatabase db = getWritableDatabase();
        String exit = " ";
        String query = " UPDATE " + TABLE_PRODUCTS+ " SET " + COLUMN_ENTRY_STATUS + "=\"" + exit + "\" WHERE " + COLUMN_NAME + "=\"" + veh + "\";";
        db.execSQL(query);

    }

    //ADDING E TO ERROR EXITS
    public void exitStatus(String veh) {

        //String veh = d.get_veh();
        //String exit = d.getExit_time();
        SQLiteDatabase db = getWritableDatabase();
        String exit = "E";
        String query = " UPDATE " + TABLE_PRODUCTS+ " SET " + COLUMN_EXIT_STATUS + "=\"" + exit + "\" WHERE " + COLUMN_NAME + "=\"" + veh + "\";";
        db.execSQL(query);

    }

    //SELECTING E ERROR EXITS
    public Cursor sendErrorExit(){

        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_ERR + " WHERE 1";
        Cursor recordSet = db.rawQuery(query, null);
        if (recordSet != null) {
            recordSet.moveToFirst();
        }
        return recordSet;
    }

    public Cursor sendTypeCell(String name){

        SQLiteDatabase db = getWritableDatabase();
        String query = ("SELECT * FROM " + TABLE_PRODUCTS+ " WHERE " +COLUMN_NAME+  "=\"" +name+ "\";" );
        Cursor recordSet = db.rawQuery(query, null);
        if (recordSet != null) {
            recordSet.moveToFirst();
        }
        return recordSet;
    }

    //REMOVING E FROM ERROR EXITS
    public void updateExitStatus(String veh) {

        //String veh = d.get_veh();
        //String exit = d.getExit_time();
        SQLiteDatabase db = getWritableDatabase();
        String exit = " ";
        String query = " UPDATE " + TABLE_PRODUCTS+ " SET " + COLUMN_EXIT_STATUS + "=\"" + exit + "\" WHERE " + COLUMN_NAME + "=\"" + veh + "\";";
        db.execSQL(query);

    }


    public void passDone(String veh, String exit, String cell){

        //String veh = d.get_veh();
        //String exit = d.getExit_time();
        SQLiteDatabase db = getWritableDatabase();
        String query = " UPDATE "+ TABLE_PASS + " SET " +COLUMN_NAME+ "=\"" +exit+ "\" , " +COLUMN_CELL_PASS+ "=\"" +cell+ "\"  WHERE " +COLUMN_PASS_NO+ "=\"" +veh+ "\";" ;
        db.execSQL(query);

    }

    public void deleteProduct(){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_PRODUCTS  );
    }

    public void deleteErrorProduct(String veh){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_ERR + " WHERE " +ERR_NAME+  "=\"" +veh+ "\";" );
    }

    public void deleteAllErrorProduct(){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_ERR  );
    }

    public void deletePass(){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_PASS  );
    }

    public void deleteOneProduct(String veh){
        SQLiteDatabase db = getWritableDatabase();
        String error = "E";
        db.execSQL("DELETE FROM " + TABLE_PRODUCTS + " WHERE " + COLUMN_NAME + "=\"" + veh + "\" ;" );
    }

    public void deleteOneProductExit(String veh){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_PRODUCTS + " WHERE " + COLUMN_NAME + "=\"" + veh + "\" ;" );
    }

}
