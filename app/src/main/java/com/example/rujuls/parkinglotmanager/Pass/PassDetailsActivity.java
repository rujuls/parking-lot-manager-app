package com.example.rujuls.parkinglotmanager.Pass;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rujuls.parkinglotmanager.Main.ExitFragment;
import com.example.rujuls.parkinglotmanager.Menu.PassListActivity;
import com.example.rujuls.parkinglotmanager.R;

public class PassDetailsActivity extends AppCompatActivity {
    String pass;
    String price;
    String time;
    String wheeler;
    String unique;
    TextView name;
    TextView dur;
    TextView type;
    TextView cost;
    public static final String PASS_UNI ="com.example.rujuls.parkinglotmanager.Pass_UNIQUE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_details);
        pass = getIntent().getStringExtra(PassListActivity.PASS_NAME);
        time = getIntent().getStringExtra(PassListActivity.PASS_DUR);
        wheeler = getIntent().getStringExtra(PassListActivity.PASS_TYPE);
        price = getIntent().getStringExtra(PassListActivity.PASS_NO);
        unique = getIntent().getStringExtra(PassListActivity.PASS_UNIQUE);
                // /Toast.makeText(this, pass, Toast.LENGTH_SHORT).show();

        name = (TextView) findViewById(R.id.textView21);
        dur = (TextView) findViewById(R.id.textView23);
        type = (TextView) findViewById(R.id.textView25);
        cost = (TextView) findViewById(R.id.textView27);

        name.setText(pass);
        dur.setText(time);
        type.setText(wheeler);
        cost.setText("Rs."+price);
    }

    public void onProceed(View v){

        Intent i = new Intent(this,PassChoiceActivity.class);
        i.putExtra(PASS_UNI,unique);
        startActivity(i);
    }
}
