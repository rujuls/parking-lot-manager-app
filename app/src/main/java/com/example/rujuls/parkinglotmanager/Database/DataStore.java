package com.example.rujuls.parkinglotmanager.Database;

/**
 * Created by RUJUL S on 15-09-2016.
 */
public class DataStore {

   private int _id;
   private String _veh;
    private String entry_time;
    private String exit_time;
    private String _type;
    private  long _cell;
    private String dur;
    private String price;
    private char entry_stat;
    private char exit_stat;

    public DataStore(){}

    public DataStore(String veh){

        this._veh = veh;
    }

    public String get_type() {
        return _type;
    }

    public void set_type(String _type) {
        this._type = _type;
    }

    public DataStore(String veh, String entry_time){

        this._veh = veh;
        this.entry_time = entry_time;
    }
    public DataStore(String veh, String entry_time,String _type ){

        this._veh = veh;
        this.entry_time = entry_time;
        this._type = _type;
    }

    public DataStore(String veh, String entry_time, String _type, long _cell){

        this._veh = veh;
        this.entry_time = entry_time;
        this._type = _type;
        this._cell = _cell;
    }



    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_veh() {
        return _veh;
    }

    public void set_veh(String _veh) {
        this._veh = _veh;
    }

    public String getEntry_time() {
        return entry_time;
    }

    public void setEntry_time(String entry_time) {
        this.entry_time = entry_time;
    }

    public String getExit_time() {
        return exit_time;
    }

    public void setExit_time(String exit_time) {
        this.exit_time = exit_time;
    }

    public long get_cell() {
        return _cell;
    }

    public void set_cell(long _cell) {
        this._cell = _cell;
    }

    public char getEntry_stat() {
        return entry_stat;
    }

    public void setEntry_stat(char entry_stat) {
        this.entry_stat = entry_stat;
    }

    public char getExit_stat() {
        return exit_stat;
    }

    public void setExit_stat(char exit_stat) {
        this.exit_stat = exit_stat;
    }

    public String getDur() {
        return dur;
    }

    public void setDur(String dur) {
        this.dur = dur;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}



